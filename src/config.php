<?php

return [

    /*
     * The routing method to use.
     *
     * Valid options are 'path' and 'domain'.
     * If 'path' is chosen, your scheme might look like 'http://yourdomain.com/docs'
     * If 'domain' is chosen, it might look like 'http://docs.yourdomain.com/'
     */
    'routing-scheme' => 'path',

    /*
     * The prefix to use for documentation routing.
     *
     * If 'path' was selected as the routing-scheme, this will be appended after the domain.
     * If 'domain' was chosen, you will need to provide the whole domain. eg, 'docs.yourdomain.com'
     */
    'routing-prefix' => 'docs',

    /*
     * This package comes with its own default view to embed the docs in.
     * You may swap this out for your own, that way you can keep your branding and color scheme consistent.
     *
     * You may pass the dot-notation path to your view, like 'layouts.docs', or you may use null to use the default one.
     *
     * The content of the page will be placed into a section called 'docs', so you will need to '@yield("docs")'
     */
    'base-view' => null,

    /*
     * The path to the root directory of your documentation markdown files.
     *
     * See the readme for more information on how to set this up.
     */
    'docs-path' => ''

];
