<?php

namespace judahnator\MdToDocs;

use Illuminate\Routing\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DocsController extends Controller
{

    /**
     * Finds the given document resource.
     *
     * @param string $view
     */
    public function index(string $view = '')
    {
        $path = config('md-to-docs.docs-path', '').'/'.$view;

        if (is_dir($path)) {
            if (file_exists($path.'/Home.md')) {
                // BitBucket uses 'Home.md' by default in their wikis
                $path .= '/Home.md';
            } elseif (file_exists($path.'/index.md')) {
                $path .= '/index.md';
            }
        }

        if (!file_exists($path) && file_exists($path.'.md')) {
            $path .= '.md';
        }

        if (!file_exists($path)) {
            throw new NotFoundHttpException('Documentation not found');
        }

        return view('md-to-docs::docs', [
                'content' => (new \Parsedown())->parse(file_get_contents($path))
            ]);
    }
}
