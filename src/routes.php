<?php

use Illuminate\Support\Facades\Route;

$routingPrefix = config('md-to-docs.routing-prefix');
$routingScheme = config('md-to-docs.routing-scheme');

switch ($routingScheme) {
    case 'path':
        Route::name('documentation')
            ->prefix($routingPrefix)
            ->get('{path?}', 'judahnator\MdToDocs\DocsController@index')
            ->where('path', '.*');
        break;

    case 'domain':
        $route = Route::name('documentation')
            ->domain($routingPrefix)
            ->get('{path?}', 'judahnator\MdToDocs\DocsController@index')
            ->where('path', '.*');
        break;

    default:
        throw new \RuntimeException("'{$routingScheme}' is not a valid routing scheme.");
}
