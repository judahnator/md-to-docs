<?php

namespace judahnator\MdToDocs;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config.php' => config_path('md-to-docs.php'),
        ]);

        // If the configuration has not been published stop here
        if (empty(config('md-to-docs', []))) {
            return;
        }

        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->loadViewsFrom(__DIR__.'/views', 'md-to-docs');
    }
}
