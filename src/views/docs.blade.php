@extends(config('md-to-docs.base-view') ?: 'md-to-docs::layout')

@section('docs')
    {!! $content !!}
@endsection