Laravel Markdown Documentation
==============================

[![pipeline status](https://gitlab.com/judahnator/md-to-docs/badges/master/pipeline.svg)](https://gitlab.com/judahnator/md-to-docs/commits/master)


What is this package?
---------------------

I made this package when I came across a project where I had a good wiki set up for user documentation, and I wanted it to be available in-app.

Unfortunately it was in a separate repository, and I did not want to translate all the markdown to HTML and keep it all in sync.

This package solves this problem.


Basic setup
-----------

First things first, you will need to require this repository into your Laravel project.

`composer require judahnator/md-to-docs`

Once loaded you will need to publish the configuration file. Run `php artisan vendor:publish` and select this package. It will create a file named `config/md-to-docs.php`

You can see all of the configuration options documented there, except for the one on pointing to your documentation files.


Your Documentation Files
------------------------

You will need to somehow get your markdown files into the project. You can either copy/paste them somewhere (`storage/docs/` might be a good place), or if they update somewhat frequently you can use composer to grab their repository.

Here is an example of how that might look in your `composer.json` file:

```json
{
    "require": {
        "project/wiki": "dev-master"
    },
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "project/wiki",
                "version": "dev-master",
                "source": {
                    "url": "git@someprovider.com:you/yourproject.git/wiki",
                    "type": "git",
                    "reference": "origin/master"
                }
            }
        }
    ]
}
```

Once you have your wiki markdown files loaded, either via composer or by simple copy/paste, point the `docs-path` configuration value in the `config/md-to-docs.php` file at the root path of your documentation.


Usage
-----

It should be fairly straightforward. Once your configuration is set browse to wherever you put your files in your browser. That might be `http://yourdomain.com/docs` or `http://docs.yourdomain.com/`, or however you set it up.

Some important notes on how this library finds your markdown files:

* You can browse to a directory in your browser. If there is an 'index.md' file in that directory, it will be served. Otherwise a 404 will be returned.
* You can omit the `.md` in your URL. `/docs/mypage.md` and `/docs/mypage` are both valid.
* Links are hit-and-miss. Still a WIP.