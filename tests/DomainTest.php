<?php

namespace judahnator\MdToDocs\Tests;

class DomainTest extends TestCase
{
    protected static function getRoutingPrefix(): string
    {
        return 'docs.localhost';
    }

    protected static function getRoutingScheme(): string
    {
        return 'domain';
    }
}
