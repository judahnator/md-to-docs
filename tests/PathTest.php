<?php

namespace judahnator\MdToDocs\Tests;

class PathTest extends TestCase
{
    protected static function getRoutingPrefix(): string
    {
        return 'docs';
    }

    protected static function getRoutingScheme(): string
    {
        return 'path';
    }
}
