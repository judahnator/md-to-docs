<?php

namespace judahnator\MdToDocs\Tests;

use judahnator\MdToDocs\ServiceProvider;
use Orchestra\Testbench\BrowserKit\TestCase as BaseTestCase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class TestCase extends BaseTestCase
{
    protected function getPackageProviders($app): array
    {
        return [ServiceProvider::class];
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     * @return void
     */
    final protected function getEnvironmentSetUp($app): void
    {
        $app['config']->set('md-to-docs', [
            'routing-scheme' => static::getRoutingScheme(),
            'routing-prefix' => static::getRoutingPrefix(),
            'base-view' => null,
            'docs-path' => __DIR__.'/test-docs'
        ]);
    }

    abstract protected static function getRoutingPrefix(): string;

    abstract protected static function getRoutingScheme(): string;

    final protected function seePageIs($uri)
    {
        $expected = 'http://';
        if (config('md-to-docs.routing-scheme') === 'domain') {
            $expected .= config('md-to-docs.routing-prefix');
        } else {
            $expected .= 'localhost/'.config('md-to-docs.routing-prefix');
        }

        $expected .= $uri;

        $this->assertEquals(
            $expected,
            $this->currentUri,
            "Did not land on expected page [{$uri}].\n"
        );

        return $this;
    }

    public function testSeeDefaultPages(): void
    {
        $this
            ->visitRoute('documentation')
            ->seePageIs('')
            ->see('foo');
    }

    public function testPathNavigation(): void
    {
        $this
            ->visitRoute('documentation', 'index')
            ->seePageIs('/index')
            ->see('foo');

        $this
            ->visitRoute('documentation', 'subdir/nested')
            ->seePageIs('/subdir/nested')
            ->see('text to find');
    }
}
